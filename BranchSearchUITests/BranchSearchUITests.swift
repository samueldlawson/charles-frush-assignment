//
//  BranchSearchUITests.swift
//  BranchSearchUITests
//
//  Created by CF on 8/6/19.
//  Copyright © 2019 CF. All rights reserved.
//

import XCTest

class BranchSearchUITests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testBranchListLoadsAndReturns() {
        let app = XCUIApplication()
        app.tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"ABERDARE")/*[[".cells.containing(.button, identifier:\"More Info, ABERDARE, ABERDARE\")",".cells.containing(.staticText, identifier:\"ABERDARE\")"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .staticText).matching(identifier: "ABERDARE").element(boundBy: 1).tap()
        app.staticTexts["HALIFAX BRANCH 14 CANON STREET"].tap()
        app.navigationBars["Branch Details"].buttons["Halifax branches"].tap()
        
    }
    
    
    
    func testDetailsControllerAppears(){
        let searchtext = "London"
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        
        let searchBarTextField = tablesQuery.searchFields[" Search by city..."]
        XCTAssertTrue(searchBarTextField.exists, "missing SearchBar")
        
        expectation(for: NSPredicate(format:("exists == 1")), evaluatedWith: searchBarTextField, handler: nil)
        
        waitForExpectations(timeout: 5.0, handler: nil)
        searchBarTextField.tap()
        searchBarTextField.typeText(searchtext)
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["LONDON SW12"]/*[[".cells.staticTexts[\"LONDON SW12\"]",".staticTexts[\"LONDON SW12\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
    }
    
    func testDetailsControllerFiledsExist(){
        XCUIApplication().tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"ABINGDON")/*[[".cells.containing(.button, identifier:\"More Info, ABINGDON, ABINGDON\")",".cells.containing(.staticText, identifier:\"ABINGDON\")"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .staticText).matching(identifier: "ABINGDON").element(boundBy: 0).tap()
        let name = XCUIApplication().staticTexts["ABINGDON"]
        XCTAssertTrue(name.exists, "expected \(name) for branch name")
        
        let address = XCUIApplication().staticTexts["HALIFAX BRANCH 8 HIGH STREET"]
        XCTAssertTrue(address.exists, "expected \(address) for branch name")
        
        let phoneNumber = XCUIApplication().staticTexts["+44-1235707015"]
        XCTAssertTrue(phoneNumber.exists, "expected \(phoneNumber) for branch name")
        
    }
    
    func testSearchBarExists() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        let searchBarTextField = tablesQuery.searchFields[" Search by city..."]
        XCTAssertTrue(searchBarTextField.exists, "missing SearchBar")
    }
    
}
