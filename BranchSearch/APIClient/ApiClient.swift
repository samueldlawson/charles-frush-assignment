//
//  APICalls.swift
//  BranchSearch
//
//  Created by CF on 8/6/19.
//  Copyright © 2019 CF. All rights reserved.
//

import Foundation

typealias BranchHandler = (_ brand: String?, _ branchData: [Branch]?, _ error: Error?, _ statusCode: String?) -> ()

class ApiClient {
    
    static let shared = ApiClient()
    
    var branchApi = "https://api.halifax.co.uk/open-banking/v2.2/branches"
    
    func getBranches(completionHandler: @escaping BranchHandler) {
        let headers = [
            "if-modified-since": "REPLACE_THIS_VALUE",
            "if-none-match": "REPLACE_THIS_VALUE",
            "accept": "application/prs.openbanking.opendata.v2.2+json"
        ]
        print(branchApi)
        let request = NSMutableURLRequest(url: NSURL(string: branchApi)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error in request: \(error as Any)")
                completionHandler(nil, nil, error, "")
            } else {
                var responseStatusCode = ""
                if let httpResponse = response as? HTTPURLResponse {
                    responseStatusCode = ("\(httpResponse.statusCode)")
                } else {
                    assertionFailure("unexpected response")
                    responseStatusCode = "unexpected response"
                }
                do {
                    let brand = try JSONDecoder().decode(Results.self, from: data!)
                    let brandName = brand.data[0].Brand[0].BrandName
                    let branch = brand.data[0].Brand[0].Branch.sorted(by: { $0.name! < $1.name! })
                    completionHandler(brandName,branch, nil, responseStatusCode)
                } catch let jsonErr{
                    print("Error serializing json: \(jsonErr)")
                    completionHandler(nil, nil, jsonErr, responseStatusCode)
                }
            }
        })
        dataTask.resume()
    }
}
