//
//  Branch.swift
//  BranchSearch
//
//  Created by CF on 8/7/19.
//  Copyright © 2019 CF. All rights reserved.
//

import Foundation
import CoreLocation

/// Results contain the data returned from the fetchBranches API call
struct Results: Decodable {
    let data: [BranchData]
}

/// BranchData contains the Brands returned in the fetchBranches API call
struct BranchData: Decodable {
    let Brand: [Brand]
}

/// Brand contains the bank name and branches array - assumed to all be the same Brand for this exercise
struct Brand: Decodable{
    let BrandName: String
    let Branch: [Branch]
}

/// Branch contains all data related to a bank's branch
struct Branch: Decodable {
    let identification: String
    let sequenceNumber: String
    let name: String?
    let type: String?
    let customerSegment: [String]?
    let otherServiceAndFacility: [OtherServiceAndFacility]?
    let availability: Availability
    let contactInfo: [ContactInfo]
    let postalAddress: PostalAddress
    
    /// handle caps in JSON
    private enum CodingKeys: String, CodingKey {
        case identification = "Identification"
        case sequenceNumber = "SequenceNumber"
        case name = "Name"
        case type = "Type"
        case customerSegment = "CustomerSegment"
        case otherServiceAndFacility = "OtherServiceAndFacility"
        case availability = "Availability"
        case contactInfo = "ContactInfo"
        case postalAddress = "PostalAddress"
    }
}

/// PostalAddress contains the Address parts for a Branch
struct PostalAddress: Decodable {
    let AddressLine: [String]?
    let Country: String?
    let CountrySubDivision: [String]?
    let GeoLocation: GeoLocation?
    let PostCode: String?
    let TownName: String?
}

/// GeoLocation contains the GeographicCoordinates for a Branch
struct GeoLocation: Decodable {
    let GeographicCoordinates: GeographicCoordinates?
}

/// GeographicCoordinates contains the Lat and Long coordinates for a branch's location.
struct GeographicCoordinates: Decodable {
    let Latitude: String?
    let Longitude: String?
    func getCoordinates() -> CLLocationCoordinate2D? {
        guard let lat = CLLocationDegrees.init(Latitude!) else {return nil}
        guard let long = CLLocationDegrees.init(Longitude!) else {return nil}
        return .init( latitude: lat, longitude: long)
    }
}

/// OtherServiceAndFacility
struct OtherServiceAndFacility: Decodable {
    let code: String?
    let name: String?
    let description: String?
    
    private enum CodingKeys: String, CodingKey {
        case code = "Code"
        case name = "Name"
        case description = "Description"
    }
}

/// Availability contains the StandardAvailability in Days for a Branch
struct Availability : Decodable {
    let StandardAvailability: Days
}

struct Days: Decodable {
    let Day: [Day]
}

/// Day contains the Hours of Operation for a Branch on a specific Day
struct Day: Decodable {
    let Name: String?
    let OpeningHours: [Hours]
}

/// Hours contain the Opening and Closing times for a Branch
struct Hours: Decodable {
    let OpeningTime: String?
    let ClosingTime: String?
}

/// ContactInfo contains the contact phone and fax numbers for a Branch
struct ContactInfo: Decodable {
    let ContactType: String?
    let ContactContent: String?
}
