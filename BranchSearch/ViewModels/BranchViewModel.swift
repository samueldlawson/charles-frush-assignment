//
//  BranchViewModel.swift
//  BranchSearch
//
//  Created by CF on 8/7/19.
//  Copyright © 2019 CF. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

/// MVVM View Model
struct BranchViewModel {
    
    let brand: String
    let name: String
    let city: String
    let address: String
    var phoneNumber: String = ""
    var coordinates: CLLocationCoordinate2D?
    let detailTextString: String
    let accessoryType: UITableViewCell.AccessoryType
    
    // Dependency Injection
    init(brand: String, branch: Branch) {
        self.brand = brand
        self.name = branch.name ?? " "
        self.city = branch.postalAddress.TownName ?? " "
        self.address = branch.postalAddress.AddressLine?[0] ?? " "
        for contact in branch.contactInfo {
            if contact.ContactType == "Phone" {
                self.phoneNumber = contact.ContactContent ?? " "
            }
        }
        self.coordinates = branch.postalAddress.GeoLocation?.GeographicCoordinates?.getCoordinates()
        detailTextString = self.city
        accessoryType = .detailDisclosureButton
    }
    
}
