//
//  ViewController.swift
//  BranchSearch
//
//  Created by CF on 8/6/19.
//  Copyright © 2019 CF. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

// MARK: - UITableViewController
class BranchesController: UITableViewController {
    private var spinner = UIActivityIndicatorView(style: .whiteLarge)
    private var branchViewModels = [BranchViewModel]()
    private var filteredResults: [BranchViewModel] = []
    private let cellId = "cellId"
    private var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupTableView()
        fetchBranches()
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
    }
}

// MARK: - Helper: Get Data
extension BranchesController {
    
    /// Get Branches from API v2.2 - simulate down system, this resets
    fileprivate func fetchBranches(){
        setupSpinner()
        ApiClient.shared.getBranches { [weak self] (brand, branches, error, status)  in
            if error != nil {
                print(error!)
                DispatchQueue.main.async { [weak self] in
                    self?.spinner.removeFromSuperview()
                    self?.showAlert("Branch search is temporarly unavailable. Please try again later.\n Code: \(status!)")
                }
            }
            guard let brandName = brand, let branchList = branches else {return}
            self?.branchViewModels = branchList.map({return BranchViewModel(brand: brandName, branch: $0)})
            DispatchQueue.main.async { [weak self] in
                self?.spinner.removeFromSuperview()
                self?.updateNavBar()
                self?.tableView.reloadData()
            }
        }
    }
}

// MARK: - TableViewDelegate and DataSource
extension BranchesController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredResults.count
        } else {
            return branchViewModels.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var branch: BranchViewModel?
        if isSearching {
            branch = filteredResults[indexPath.row]
        } else {
            branch = branchViewModels[indexPath.row]
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! BranchCell
        cell.branchViewModel = branch
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showBranchDetails(indexPath)
    }
    
    override func tableView(_ tableView: UITableView,
                            accessoryButtonTappedForRowWith indexPath: IndexPath){
        showBranchDetails(indexPath)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // TODO: move to constants file 
        return 44
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let searchBarView = UISearchBar()
        searchBarView.searchBarStyle = UISearchBar.Style.prominent
        searchBarView.placeholder = " Search by city..."
        searchBarView.sizeToFit()
        searchBarView.isTranslucent = false
        searchBarView.backgroundImage = UIImage()
        searchBarView.delegate = self
        return searchBarView
    }
}

// MARK: - Setup UI
extension BranchesController {
    
    fileprivate func setupSpinner() {
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    fileprivate func setupTableView() {
        tableView.register(BranchCell.self, forCellReuseIdentifier: cellId)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        tableView.separatorColor = .mainTextBlue
        tableView.backgroundColor = UIColor.rgb(r: 12, g: 47, b: 57)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
        tableView.tableFooterView = UIView()
        tableView.sectionHeaderHeight = 44
        tableView.keyboardDismissMode = .onDrag
    }
    
    fileprivate func setupNavBar() {
        navigationItem.title = "Loading branches..."
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.backgroundColor = .yellow
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.rgb(r: 50, g: 199, b: 242)
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    fileprivate func updateNavBar(){
        navigationItem.title = "\(branchViewModels[0].brand) branches"
    }
    
}

// MARK: - Navigation
extension BranchesController {
    fileprivate func showBranchDetails(_ indexPath: IndexPath) {
        var selectedRow: BranchViewModel
        if isSearching {
            selectedRow = filteredResults[indexPath.row]
        } else {
            selectedRow = branchViewModels[indexPath.row]
        }
        let detailsController = UIStoryboard.controllerFromMainStoryboard(cls: DetailsController.self)
        detailsController.branch = selectedRow
        self.navigationController?.pushViewController(detailsController, animated: true)
    }
    
    /// show alert if system is down
    /// - to test change the ApiClient.shared.branchApi to an invalid url ex: "https://api.halifax.co.uk/open-banking/v2.2/branchesBAD"
    func showAlert(_ message: String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { [weak self]  action in
            switch action.style{
            case .default:
                print("default")
                ApiClient.shared.branchApi = "https://api.halifax.co.uk/open-banking/v2.2/branches"
                self?.fetchBranches()
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }})
        )
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - SearchBar Delegate
extension BranchesController: UISearchBarDelegate, UITextFieldDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange textSearched: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            tableView.reloadData()
        } else {
            isSearching = true
            filteredResults = branchViewModels.filter({$0.city.range(of:searchBar.text!, options: .caseInsensitive) != nil})
            tableView.reloadData()
        }
    }
}

