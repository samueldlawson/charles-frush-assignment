//
//  DetailsController.swift
//  BranchSearch
//
//  Created by CF on 8/7/19.
//  Copyright © 2019 CF. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DetailsController: UIViewController {
    
    /// branch (ViewModel) contains all necessary data elements to populate the labels
    public var branch: BranchViewModel?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    // TODO: Verify with team if City is required and add it if needed
    
    override func viewDidLoad() {
        super .viewDidLoad()
        setupNavBar()
        configureLabels()
        setupMapView()
        addAnnotation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super .viewDidDisappear(animated)
        removeMap()
    }
}

//MARK: - Setup
extension DetailsController {
    
    /// use the ViewModel to populate labels
    /// 1. Name
    /// 2. Address
    /// 3. Phone number
    fileprivate func configureLabels(){
        nameLabel.text = branch?.name
        addressLabel.text = branch?.address
        phoneNumberLabel.text = branch?.phoneNumber
    }
    
    
    fileprivate func setupNavBar() {
        navigationItem.title = "Branch Details"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.backgroundColor = .black
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.rgb(r: 50, g: 199, b: 242)
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
}


// MARK: - MapView functionality (out of scope)
// to remove
// 1: delete the extension below
// 2: delete the mapView from the storybaord and IBOutlet
// 3: delete the call to removeMap() in viewDidDisappear
extension DetailsController: MKMapViewDelegate {
    
    fileprivate func setupMapView(){
        mapView.delegate = self
        mapView.isUserInteractionEnabled = false
    }
    
    fileprivate func removeMap(){
        let annotatins = mapView.annotations
        mapView.removeAnnotations(annotatins)
        mapView.delegate = nil
        mapView = nil
    }
    
    fileprivate func addAnnotation(){
        if let coord = branch?.coordinates {
            centerMapOnLocation(coord, mapView: mapView)
            let annotation = MKPointAnnotation()
            annotation.title = branch?.name
            annotation.subtitle = branch?.address
            annotation.coordinate = coord
            mapView.addAnnotation(annotation)
            mapView.selectAnnotation(annotation, animated: true)
        }
    }
    
    fileprivate func centerMapOnLocation(_ location: CLLocationCoordinate2D, mapView: MKMapView) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion(center: location,
                                                  latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    fileprivate func  mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{
            return nil;
        } else {
            let pinIdent = "Pin";
            var pinView: MKPinAnnotationView;
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation;
                pinView = dequeuedView;
            }else{
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: pinIdent);
                
            }
            return pinView;
        }
    }
}
