//
//  NavigationController.swift
//  BranchSearch
//
//  Created by CF on 8/7/19.
//  Copyright © 2019 CF. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
