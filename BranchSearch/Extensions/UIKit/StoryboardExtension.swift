//
//  StoryboardExtension.swift
//  BranchSearch
//
//  Created by CF on 8/7/19.
//  Copyright © 2019 CF. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    // return viewControlle from main storyboard
    static func controllerFromMainStoryboard<T>(cls: T.Type) -> T {
        return self.controllerFromStoryboard("Main", cls: cls)
    }
    
    // return viewController based on in class from storybourd with particular name
    static func controllerFromStoryboard<R>(_ name: String, cls: R.Type) -> R {
        let stringFromClass = String(describing: cls)
        let storyboard = UIStoryboard(name: name, bundle: nil)
        
        guard let controller = storyboard.instantiateViewController(withIdentifier: stringFromClass) as? R else {
            fatalError("Expected viewController to be of type \(R.self)")
        }
        
        return controller
    }
    
    static func initialControllerFromStoryboard<R>(_ name: String) -> R? {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        return storyboard.instantiateInitialViewController() as? R
    }
    
    class func viewController(_ storyboard: String, identifier: String) -> UIViewController {
        return UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
}

