//
//  BranchSearchTests.swift
//  BranchSearchTests
//
//  Created by CF on 8/6/19.
//  Copyright © 2019 CF. All rights reserved.
//

import XCTest
@testable import BranchSearch

class BranchSearchTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testBranchApiFailure() {
        let expectation = XCTestExpectation(description: "Force failure on API call - simulate down system")
        let badURL = "https://api.halifax.co.uk/open-banking/v2.2/branchesBAD"
        let statusCodeExpected = "404"
        ApiClient.shared.branchApi = badURL
        ApiClient.shared.getBranches{ (brand, branches, error, statusCode)  in
            if statusCode != nil {
                XCTAssertEqual(statusCode, statusCodeExpected, "statusCode expected \(statusCodeExpected) on simulated bad url but received \(statusCode!)")
            }
            
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testBranchApiSuccess() {
        let expectation = XCTestExpectation(description: "Force failure on API call - simulate down system")
        let badURL = "https://api.halifax.co.uk/open-banking/v2.2/branches"
        let statusCodeExpected = "200"
        ApiClient.shared.branchApi = badURL
        ApiClient.shared.getBranches{ (brand, branches, error, statusCode)  in
            if statusCode != nil {
                XCTAssertEqual(statusCode, statusCodeExpected, "statusCode expected \(statusCodeExpected) on simulated bad url but received \(statusCode!)")
            }
            
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testBranchsReturnedAnsCoordinates() {
        self.measure {
            let expectation = XCTestExpectation(description: "Download BranchList from API")
            let latitude = "51.713684"
            let longitude = "-3.445321"
            let branchCount = 598
            ApiClient.shared.getBranches{ (brand, branches, error, statusCode)  in
                if error != nil {
                    print(error!)
                    XCTAssert(false)
                }
                guard let branchList = branches else {
                    XCTAssert(false)
                    return}
                XCTAssertEqual(branchCount, branchList.count)
                XCTAssertEqual(latitude, branchList[0].postalAddress.GeoLocation?.GeographicCoordinates?.Latitude)
                XCTAssertEqual(longitude,  branchList[0].postalAddress.GeoLocation?.GeographicCoordinates?.Longitude)
                expectation.fulfill()
            }
            wait(for: [expectation], timeout: 10.0)
        }
    }
}
